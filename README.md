# SSM（Spring+SpringMVC+Mybatis）框架学习（一）Spring学习之Spring开发步骤及Bean实例化配置

>  说明：本项目是个人学习SSM框架技术过程中的一些案例，以便用来更好的理解某个细节知识。通过参考视频学习进行总结的，如有雷同者还望谅解，目的知识为了让学习记忆更深！还有很多需要学习的，不足的地方希望大家多多指教！
# 第一章   Spring学习
### 1.1  搭建Spring项目的步骤
1.创建Spring坐标，即导入spring依赖
```xml
<!--    pom.xml 添加Spring依赖-->
<dependency>
	<groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.3.7</version> 
</dependency>
```
2.创建Bean<br>
3.创建配置文件applicationContext.xml<br>
4.配置配置文件
```xml
<Bean id="UserDao" class="com.litianliu.spring.UserDao.Impl"></Bean>
```
5.创建applicationContext对象getBean
```java
//applicationContext.xml是我创建的xml配置文件，可根据你自己创建的自定义
public class springTest {
    public static void main(String[] args){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao= (UserDao) app.getBean("userDao");
        userDao.save();
    }
}
```
### 1.2  Spring 配置文件详解
#### 1.2.1 Bean 标签的基本配置 id class<br>
- id 唯一表示Bean实例的，可随意起，但不能重复，一般是类名
- class 指向Bean实例化所在的package包名
#### 1.2.2 Bean 标签的范围配置 scope 理解及对比

scope属性         | singtype    | prototype    |
:-----------------: |:-------- |:-------- |
实例个数         | 1个  | 多个 |
实例时机        | spring核心文件加载时，实例化配置的Bean实例  | 当调用getBean()方法时实例化Bean实例 |
生命周期   | 对象创建：适用对象时创建新的实例对象<br>对象运行：只要对象在使用，对象就一直活着 <br>对象销毁：当应用卸载时，销毁容器时，对象就被销毁了| 对象创建：当应用加载，创建容器时对象就被创建了<br>对象运行：只要容器在，对象就一直活着 <br>对象销毁：当对象长时间不用时，被java的垃圾回收器回收了  |
#### 1.2.3 Bean 生命周期的配置
- init-method：指定类中初始化方法的名称
- destory-method：指定类中销毁方法的名称

#### 1.2.4 Bean 实例化的三种方式
- 无参构造方法实例化
```xml
  <bean id="userDao" class="com.litianliu.spring.Dao.Impl.UserDaoImpl"></bean>
 ```
- 工厂静态方法实例化<br>
(1)xml配置
```xml
  <bean id="user" class="com.litianliu.spring.factory.staticFactory" factory-method="getUser"></bean>
 ```
(2)创建对象
```java
@Data
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private int age;
}
```
(3)静态方法
```java
private static Map<Integer, User> userMap;
static {
    userMap=new HashMap<Integer, User>();
    userMap.put(1,new User("1","张三",22));
    userMap.put(2,new User("2","李四",24));
}
public static User getUser(Integer num){
    return userMap.get(num);
}
```
(4)测试
```java
 @Test
public void staticFactory(){
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    User user= (User) app.getBean("user");
    System.out.println(user);
}
```

- 工厂实例方法实例化

(1)xml配置
```xml
<bean id="dynameic" class="com.litianliu.spring.factory.DynamicFactory"></bean>
<bean id="user" factory-bean="dynameic" factory-method="getUser">
    <constructor-arg value="2"></constructor-arg>
</bean>
```
(2)创建对象
```java
@Data
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private int age;
}
```
(3)静态方法
```java
public class DynamicFactory {
    private static Map<Integer, User> userMap;
    public DynamicFactory(){
        userMap=new HashMap<Integer, User>();
        userMap.put(1,new User("1","张三",22));
        userMap.put(2,new User("2","李四",24));
    }
    public User getUser(Integer num){
        return userMap.get(num);
    }
}
```
(4)测试
```java
 @Test
public void staticFactory(){
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    User user= (User) app.getBean("user");
    System.out.println(user);
}
```
工厂静态方法和实例方法实例化bean的区别：
静态方法创建对象，不需要实例化工厂对象，因为静态工厂的静态方法，不需要创建对象即可调用，在spring的xml里只需创建一个bean

实例化方法创建对象，需要实例化工厂对象，因为它的方法是静态的，必须要通过实例化才能调用，必须要创建工厂对象，在spring的xml文件中需要创建两个bean，一个是工厂bean，一个是队形bean

> **记录学习的点点滴滴，每天收获一点知识，努力让自己变得更优秀！学习过程中有学习记录源码，学习过程中不断更新，有兴趣的小伙伴可以一起学习，一起讨论哦！
学习源码 gitlab 地址: [点击这里](https://gitlab.com/Archer405-dev/ssmstudy.git).**

## 1.3 spring注解开发
### 1.3.1 spring注解开发之bean自动注入
步骤：（1）配置自动扫包
     （2）在类上添加注解
- 在applicationContext.xml中添加扫包代码
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
    <context:component-scan base-package="com.litianliu.spring"></context:component-scan>
</beans>
```
- 在类上添加注解

a.创建一个实体类DateSourse.java并添加自动注入@Component，@Date是lombox所带注解，主要作用是自动生成setter，getter方法
```java
@Component
@Data
public class DateSourse {
    @Value("com.mysql.cj.jdbc.driver")
    private String driver;
    @Value("root")
    private String username;
    @Value("llxxuin")
    private String password;
    @Value("jdbc:mysql://www.litianliu.top:3306/ermsSystem")
    private String url;
}
```
注：
- @Value("")是直接给字段赋值

- 自动注入的两种方式：byType和byName，默认都是通过byType的，即名称为类名但是首字母转化为小写


b.创建测试类并验证自动注入是否成功
```java
@Test
public void datesourse() {
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    DateSourse dateSourse= (DateSourse) app.getBean("dateSourse");
    System.out.println(dateSourse);
}
```
component注解是将标注的类加载到容器中，实际开发中可以根据需求使用controller，service，respository来标注控制类你服务类和数据交互类

## 1.4 spring IOC底层实现

技术点：xml解析 + 反射
> 思路：<br>
> 1.根据需求编写 xml 文件，配置需要创建的 bean<br>
> 2.编写程序读取 xml 文件，获取 bean 相关信息、类、属性、id<br>
> 3.根据第二步获取到的信息，结合反射机制动态创建对象，同时完成属性的赋值<br>
> 4.将创建好的 bean 存入Map集合，设置key-value映射，key就是bean中的id值，value就是bean对象<br>
> 5.提供方法从Map中通过id获取到对应的value<br>
```java
package com.litianliu.spring.Ioc;

import com.sun.xml.internal.ws.server.ServerRtException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * @author tl.li
 * @since 2021/8/10 16:48
 */
public class MyClassPathXmlApplicatuonContext implements ApplicationContext {
    private Map<String,Object> map;
    public MyClassPathXmlApplicatuonContext(String path){
        map=new HashMap<String, Object>();
        //解析xml
        parseXML("src/main/resources/"+path);
    }

    private void parseXML(String path) {
        SAXReader saxReader=new SAXReader();
        try {
            Document document=saxReader.read(path);
            Element root=document.getRootElement();
            Iterator<Element> rootIter= root.elementIterator();
            while (rootIter.hasNext()){
                Element bean=rootIter.next();
                String id=bean.attributeValue("id");
                String clazz=bean.attributeValue("class");
                //反射动态创建对象
                Class clazzz=Class.forName(clazz);
                Constructor constructor=clazzz.getConstructor();
                Object object=constructor.newInstance();
                //给属性赋值
                Iterator<Element> beanIter=bean.elementIterator();
                while (beanIter.hasNext()){
                    Element propery=beanIter.next();
                    String properyName=propery.attributeValue("name");
                    String properyValue=propery.attributeValue("value");
                    //获取setter方法
                    //id-setId,name-setName,age-detAge
                    String methodName="set"+properyName.substring(0,1).toUpperCase()+properyName.substring(1);
                    //获取属性类型
                    Field field=clazzz.getDeclaredField(properyName);
                    Method method=clazzz.getMethod(methodName,field.getType());
                    Object value=properyValue;
                    //类型转换
                   switch (field.getType().getName()){
                       case "java.lang.Integer":value=Integer.parseInt(properyValue);
                       break;
                   }
                    //调用方法
                    method.invoke(object,value);
                }
                map.put(id,object);
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return null;
    }

    public String getApplicationName() {
        return null;
    }

    public String getDisplayName() {
        return null;
    }

    public long getStartupDate() {
        return 0;
    }

    public ApplicationContext getParent() {
        return null;
    }

    public AutowireCapableBeanFactory getAutowireCapableBeanFactory() throws IllegalStateException {
        return null;
    }

    public BeanFactory getParentBeanFactory() {
        return null;
    }

    public boolean containsLocalBean(String s) {
        return false;
    }

    public boolean containsBeanDefinition(String s) {
        return false;
    }

    public int getBeanDefinitionCount() {
        return 0;
    }

    public String[] getBeanDefinitionNames() {
        return new String[0];
    }

    public <T> ObjectProvider<T> getBeanProvider(Class<T> aClass, boolean b) {
        return null;
    }

    public <T> ObjectProvider<T> getBeanProvider(ResolvableType resolvableType, boolean b) {
        return null;
    }

    public String[] getBeanNamesForType(ResolvableType resolvableType) {
        return new String[0];
    }

    public String[] getBeanNamesForType(ResolvableType resolvableType, boolean b, boolean b1) {
        return new String[0];
    }

    public String[] getBeanNamesForType(Class<?> aClass) {
        return new String[0];
    }

    public String[] getBeanNamesForType(Class<?> aClass, boolean b, boolean b1) {
        return new String[0];
    }

    public <T> Map<String, T> getBeansOfType(Class<T> aClass) throws BeansException {
        return null;
    }

    public <T> Map<String, T> getBeansOfType(Class<T> aClass, boolean b, boolean b1) throws BeansException {
        return null;
    }

    public String[] getBeanNamesForAnnotation(Class<? extends Annotation> aClass) {
        return new String[0];
    }

    public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> aClass) throws BeansException {
        return null;
    }

    public <A extends Annotation> A findAnnotationOnBean(String s, Class<A> aClass) throws NoSuchBeanDefinitionException {
        return null;
    }

    public Object getBean(String s) throws BeansException {
        return map.get(s);
    }

    public <T> T getBean(String s, Class<T> aClass) throws BeansException {
        return null;
    }

    public Object getBean(String s, Object... objects) throws BeansException {
        return null;
    }

    public <T> T getBean(Class<T> aClass) throws BeansException {
        return null;
    }

    public <T> T getBean(Class<T> aClass, Object... objects) throws BeansException {
        return null;
    }

    public <T> ObjectProvider<T> getBeanProvider(Class<T> aClass) {
        return null;
    }

    public <T> ObjectProvider<T> getBeanProvider(ResolvableType resolvableType) {
        return null;
    }

    public boolean containsBean(String s) {
        return false;
    }

    public boolean isSingleton(String s) throws NoSuchBeanDefinitionException {
        return false;
    }

    public boolean isPrototype(String s) throws NoSuchBeanDefinitionException {
        return false;
    }

    public boolean isTypeMatch(String s, ResolvableType resolvableType) throws NoSuchBeanDefinitionException {
        return false;
    }

    public boolean isTypeMatch(String s, Class<?> aClass) throws NoSuchBeanDefinitionException {
        return false;
    }

    public Class<?> getType(String s) throws NoSuchBeanDefinitionException {
        return null;
    }

    public Class<?> getType(String s, boolean b) throws NoSuchBeanDefinitionException {
        return null;
    }

    public String[] getAliases(String s) {
        return new String[0];
    }

    public void publishEvent(Object o) {

    }

    public String getMessage(String s, Object[] objects, String s1, Locale locale) {
        return null;
    }

    public String getMessage(String s, Object[] objects, Locale locale) throws NoSuchMessageException {
        return null;
    }

    public String getMessage(MessageSourceResolvable messageSourceResolvable, Locale locale) throws NoSuchMessageException {
        return null;
    }

    public Environment getEnvironment() {
        return null;
    }

    public Resource[] getResources(String s) throws IOException {
        return new Resource[0];
    }

    public Resource getResource(String s) {
        return null;
    }

    public ClassLoader getClassLoader() {
        return null;
    }
}
```

