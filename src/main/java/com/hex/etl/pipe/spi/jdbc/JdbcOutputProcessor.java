//jdjdjdjd

////
//// Source code recreated from a .class file by IntelliJ IDEA
//// (powered by FernFlower decompiler)
////
//
//package com.hex.etl.pipe.spi.jdbc.processor;
//
//import com.hex.etl.pipe.OutputProcessor;
//import com.hex.etl.pipe.PipeContext;
//import com.hex.etl.pipe.exception.EngineRuntimeException;
//import com.hex.etl.pipe.model.DataRow;
//import com.hex.etl.pipe.model.DataRowStream;
//import com.hex.etl.pipe.model.PipeReport;
//import com.hex.etl.pipe.spi.file.TextLineRowStream;
//import com.hex.etl.pipe.spi.jdbc.JdbcMetaField;
//import com.hex.etl.pipe.spi.jdbc.JdbcOutputSource;
//import java.io.IOException;
//import java.sql.Connection;
//import java.sql.PreparedStatement;
//import java.sql.SQLException;
//import java.text.DateFormat;
//import java.util.ArrayList;
//import java.util.Date;
//import java.util.Iterator;
//import java.util.List;
//import java.util.Locale;
//import java.util.Map;
//import java.util.regex.Matcher;
//import org.apache.commons.lang3.StringUtils;
//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;
//import org.springframework.dao.DataAccessException;
//import org.springframework.jdbc.core.ConnectionCallback;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.jdbc.support.JdbcUtils;
//
//public class JdbcOutputProcessor extends JdbcProcessor implements OutputProcessor<JdbcOutputSource> {
//    private static final Logger logger = LogManager.getLogger(JdbcOutputProcessor.class);
//    public static final JdbcOutputProcessor me = new JdbcOutputProcessor();
//
//    public JdbcOutputProcessor() {
//    }
//
//    public void processOutput(JdbcOutputSource output, DataRowStream rowStream, PipeContext pipeContext) {
//        JdbcTemplate jdbcTemplate = new JdbcTemplate(output.getDataSource());
//        String targetTable = output.getTableName();
//        boolean hisTableMode = false;
//        if (StringUtils.isNotEmpty(output.getHisTableName()) && output.getBatchPkIdField() != null) {
//            targetTable = output.getHisTableName();
//            hisTableMode = true;
//        }
//
//        String batchPkId = null;
//        if (hisTableMode) {
//            batchPkId = pipeContext.getParamValue(output.getBatchPkIdField(), "");
//            if (StringUtils.isEmpty(batchPkId)) {
//                throw new EngineRuntimeException("invalid batchPkId value! fieldId=" + output.getBatchPkIdField());
//            }
//        }
//
//        Map<String, JdbcMetaField> metaFields = this.getTableMetaData(jdbcTemplate, targetTable);
//        String[] targetFields = rowStream.getFields();
//        String insertSql = this.buildInsertSql(targetTable, targetFields);
//        int batchSize = output.getBatchSize();
//        ArrayList<Object[]> batchList = new ArrayList(batchSize);
//        long sumRows = 0L;
//        int[] sqlTypes = new int[targetFields.length];
//
//        for(int i = 0; i < sqlTypes.length; ++i) {
//            String fieldId = targetFields[i].toUpperCase();
//            sqlTypes[i] = ((JdbcMetaField)metaFields.get(fieldId)).getJdbcType();
//        }
//
//        PipeReport pipeReport = pipeContext.getPipeReport();
//        TextLineRowStream rowsOne = (TextLineRowStream)rowStream;
//
//        try {
//            pipeReport.addReadRows(Integer.parseInt(rowsOne.readLine()));
//        } catch (IOException var22) {
//            this.onFailed(output, batchList, pipeReport, var22);
//            return;
//        }
//
//        boolean first = true;
//
//        while(rowStream.hasMoreElements()) {
//            try {
//                DataRow dataRow = rowStream.nextElement();
//                if (null != dataRow) {
//                    this.formatRowData(targetFields, dataRow, metaFields);
//                    if (first) {
//                        if (!hisTableMode) {
//                            this.processSaveBefore(jdbcTemplate, output, rowStream, dataRow);
//                        }
//
//                        first = false;
//                    }
//
//                    batchList.add(dataRow.getData());
//                }
//            } catch (Throwable var21) {
//                this.onFailed(output, batchList, pipeReport, var21);
//                return;
//            }
//        }
//
//        if (batchList.size() > 0) {
//            try {
//                List<Object[]> tempList = new ArrayList(batchSize);
//
//                for(int i = 0; i < batchList.size(); ++i) {
//                    logger.info("begin write rows to jdbc! SQL={}", new Object[]{this.buildPrintSql(insertSql, (Object[])batchList.get(i), sqlTypes)});
//                    tempList.add(batchList.get(i));
//                    if (i % batchSize == 0 || i == batchList.size() - 1) {
//                        jdbcTemplate.batchUpdate(insertSql, tempList);
//                        sumRows += (long)tempList.size();
//                        pipeReport.addSaveRows(tempList.size());
//                        tempList.clear();
//                    }
//                }
//            } catch (Throwable var23) {
//                this.onFailed(output, batchList, pipeReport, var23);
//            }
//        }
//
//        if (hisTableMode) {
//            this.insertDistinctFromHisTable(jdbcTemplate, output.getHisTableName(), output.getTableName(), output.getDeleteBeforeFields(), output.getBatchPkIdField(), batchPkId);
//        }
//
//        logger.info("end write data rows to jdbc! rows={}", new Object[]{sumRows});
//    }
//
//    private void insertDistinctFromHisTable(JdbcTemplate jdbcTemplate, String hisTableName, String tableName, String[] pkFields, String batchPkIdField, final String batchPkId) {
//        final String deleteSql = this.processDeleteSql(hisTableName, tableName, pkFields, batchPkIdField);
//        final String insertSql = this.processInsertSql(hisTableName, tableName, pkFields, batchPkIdField);
//        jdbcTemplate.execute(new ConnectionCallback<Object>() {
//            public Object doInConnection(Connection con) throws SQLException, DataAccessException {
//                con.setAutoCommit(false);
//
//                try {
//                    PreparedStatement psDel = null;
//
//                    try {
//                        psDel = con.prepareStatement(deleteSql);
//                        psDel.setString(1, batchPkId);
//                        psDel.execute();
//                    } finally {
//                        JdbcUtils.closeStatement(psDel);
//                    }
//
//                    PreparedStatement psInsert = null;
//
//                    try {
//                        psInsert = con.prepareStatement(insertSql);
//                        psInsert.setString(1, batchPkId);
//                        psInsert.setString(2, batchPkId);
//                        psInsert.execute();
//                    } finally {
//                        JdbcUtils.closeStatement(psInsert);
//                    }
//
//                    con.commit();
//                } catch (Throwable var13) {
//                    con.rollback();
//                }
//
//                return null;
//            }
//        });
//    }
//
//    private String processDeleteSql(String hisTableName, String currentTable, Object[] pkFields, String batchFieldId) {
//        StringBuilder sql = new StringBuilder(512);
//        sql.append("DELETE FROM  ").append(currentTable).append(" C WHERE exists ( SELECT 1 FROM  ").append(hisTableName).append(" H  WHERE ");
//        sql.append(" H.").append(batchFieldId).append(" = ? ");
//
//        for(int i = 0; i < pkFields.length; ++i) {
//            sql.append(" AND H.").append(pkFields[i]).append("= C.").append(pkFields[i]);
//        }
//
//        sql.append(")");
//        logger.info("DELETE CURRENTTABLE  SQL:{}", new Object[]{sql});
//        return sql.toString();
//    }
//
//    private String processInsertSql(String hisTableName, String currentTable, Object[] pkFields, String batchFieldId) {
//        StringBuilder sql = new StringBuilder(512);
//        sql.append("INSERT INTO  ").append(currentTable).append(" SELECT H1.* FROM ").append(hisTableName).append(" H1 ");
//        sql.append("INNER JOIN ( SELECT MAX(PK_ID) AS PK_ID FROM ").append(hisTableName).append(" WHERE ").append(batchFieldId).append(" = ? ");
//        sql.append("GROUP BY ");
//
//        for(int i = 0; i < pkFields.length - 1; ++i) {
//            sql.append(pkFields[i]).append(",");
//        }
//
//        sql.append(pkFields[pkFields.length - 1] + " ) H2 ON H1.PK_ID = H2.PK_ID AND H1.").append(batchFieldId).append(" = ? ");
//        logger.info("INSERT CURRENTTABLE SQL:{}", new Object[]{sql});
//        return sql.toString();
//    }
//
//    private void onFailed(JdbcOutputSource output, ArrayList<Object[]> batchList, PipeReport pipeReport, Throwable e) {
//        if (output.isExitOnError()) {
//            throw new EngineRuntimeException(e);
//        } else {
//            pipeReport.getFailInfo().addExceptions(e);
//            pipeReport.addFailedRows(batchList.size() > 0 ? batchList.size() : 1);
//            batchList.clear();
//        }
//    }
//
//    private void formatRowData(String[] fields, DataRow dataRow, Map<String, JdbcMetaField> metaFields) {
//        Object[] data = dataRow.getData();
//
//        for(int i = 0; i < data.length; ++i) {
//            String fieldId = fields[i].toUpperCase();
//            JdbcMetaField field = (JdbcMetaField)metaFields.get(fieldId);
//            if (null != field) {
//                data[i] = this.formatFieldValue(data[i], field);
//            }
//        }
//
//    }
//
//    private Object formatFieldValue(Object value, JdbcMetaField field) {
//        return value;
//    }
//
//    private void processSaveBefore(JdbcTemplate jdbcTemplate, JdbcOutputSource output, DataRowStream rowStream, DataRow dataRow) {
//        String[] deleteBeforeFields = output.getDeleteBeforeFields();
//        if (null != deleteBeforeFields && deleteBeforeFields.length != 0) {
//            StringBuilder sql = new StringBuilder(128);
//            sql.append("DELETE FROM ").append(output.getTableName()).append(" WHERE ").append(deleteBeforeFields[0]).append("=?");
//
//            for(int i = 1; i < deleteBeforeFields.length; ++i) {
//                sql.append(" AND ").append(deleteBeforeFields[i]).append("=?");
//            }
//
//            Object[] args = new Object[deleteBeforeFields.length];
//
//            int deletedCount;
//            for(deletedCount = 0; deletedCount < deleteBeforeFields.length; ++deletedCount) {
//                args[deletedCount] = dataRow.getField(rowStream.getFieldIndex(deleteBeforeFields[deletedCount]));
//            }
//
//            deletedCount = jdbcTemplate.update(sql.toString(), args);
//            logger.info("DELETE BEFORE ROWS:{}, SQL:{}, ARGS:{}", new Object[]{deletedCount, sql, StringUtils.join(args, ",")});
//        }
//    }
//
//    private String buildInsertSql(String tableName, String[] fields) {
//        StringBuilder sql = new StringBuilder(512);
//        sql.append("INSERT INTO ").append(tableName).append("(").append(fields[0]);
//
//        int i;
//        for(i = 1; i < fields.length; ++i) {
//            sql.append(",").append(fields[i]);
//        }
//
//        sql.append(") VALUES(?");
//
//        for(i = 1; i < fields.length; ++i) {
//            sql.append(",?");
//        }
//
//        sql.append(")");
//        return sql.toString();
//    }
//
//    private String buildInsertSqlUnit(String tableName, String[] deleteBeforeFields, String hisTableName, ArrayList<Object[]> batchList) {
//        Iterator var5 = batchList.iterator();
//
//        while(var5.hasNext()) {
//            Object[] object = (Object[])var5.next();
//
//            for(int i = 0; i < object.length; ++i) {
//                object[i].toString();
//            }
//        }
//
//        StringBuilder sql = new StringBuilder(512);
//        sql.append("INSERT INTO ").append(tableName).append(" select * from ").append(hisTableName).append(" h");
//        sql.append(" where h.").append("BATCH_NO ").append(" =");
//        return sql.toString();
//    }
//
//    public String buildPrintSql(String sql, Object[] data, int[] sqqlTypes) {
//        sql = sql.replaceAll("[\\s\n ]+", " ");
//
//        int i;
//        for(i = 0; i < data.length; ++i) {
//            sql = sql.replaceFirst("\\?", "_sql_param_" + i);
//        }
//
//        for(i = 0; i < data.length; ++i) {
//            if (data[i] == null || data[i].toString().length() == 0) {
//                sql = sql.replaceFirst("_sql_param_" + i, "null");
//            } else if (sqqlTypes[i] == 12) {
//                String value = "'" + data[i].toString() + "'";
//                sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(value));
//            } else if (sqqlTypes[i] == 93) {
//                DateFormat formatter = DateFormat.getDateTimeInstance(2, 2, Locale.CHINA);
//                String value = "'" + formatter.format((Date)data[i]) + "'";
//                sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(value));
//            } else {
//                sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(data[i].toString()));
//            }
//        }
//
//        return sql;
//    }
//}
