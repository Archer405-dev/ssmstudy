package com.litianliu.spring.factory;

import com.litianliu.spring.Dao.Impl.UserDaoImpl;
import com.litianliu.spring.Dao.UserDao;
import com.litianliu.spring.Entity.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr li
 * @date 2021/8/1 18:24
 */
public class staticFactory {
    /**
       * @things 静态工厂实例化Bean，创建静态工厂
       * @author Mr li
    　　* @date 2021/8/1 18:25
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
//    private static Map<Integer, User> userMap;
//    static {
//        userMap=new HashMap<Integer, User>();
//        userMap.put(1,new User("1","张三",22));
//        userMap.put(2,new User("2","李四",24));
//    }
//    public static User getUser(Integer num){
//        return userMap.get(num);
//    }

    public static UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
