package com.litianliu.spring.factory;

import com.litianliu.spring.Dao.Impl.UserDaoImpl;
import com.litianliu.spring.Dao.UserDao;
import com.litianliu.spring.Entity.User;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Mr li
 * @date 2021/8/1 18:30
 */
public class DynamicFactory {
    /**
       * @things 工厂实例方法实例化Bean
       * @author Mr li
    　　* @date 2021/8/1 18:31
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
//    private static Map<Integer, User> userMap;
//    public DynamicFactory(){
//        userMap=new HashMap<Integer, User>();
//        userMap.put(1,new User("1","张三",22));
//        userMap.put(2,new User("2","李四",24));
//    }
//    public User getUser(Integer num){
//        return userMap.get(num);
//    }

    public UserDao getUserDao(){
        return new UserDaoImpl();
    }
}
