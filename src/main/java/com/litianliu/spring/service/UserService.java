package com.litianliu.spring.service;

import org.springframework.stereotype.Component;

/**
 * @author tl.li
 * @since 2021/8/10 15:40
 */
public interface UserService {
    String getGragle(double gradle);
}
