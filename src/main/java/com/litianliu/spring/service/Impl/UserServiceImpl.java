package com.litianliu.spring.service.Impl;

import com.litianliu.spring.Dao.UserDao;
import com.litianliu.spring.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * @author tl.li
 * @since 2021/8/10 15:40
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserDao userDao;
    public String getGragle(double gradle) {
        return userDao.getResult(gradle);
    }
}
