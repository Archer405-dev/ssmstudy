package com.litianliu.spring.controller;

import com.litianliu.spring.service.UserService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

/**
 * @author tl.li
 * @since 2021/8/10 8:53
 */
@Controller
public class UserController {
    @Autowired
    private UserService userService;
    public String getUser(double gradle){
        return userService.getGragle(gradle);
    }
}
