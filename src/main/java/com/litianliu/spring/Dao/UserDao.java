package com.litianliu.spring.Dao;

import org.springframework.stereotype.Component;

/**
 * @author Mr li
 * @date 2021/8/1 16:25
 */
public interface UserDao {
    /**
       * @things save方法
       * @author Mr li
    　　* @date 2021/8/1 16:48
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    void save();
    /**
       * @things 初始化Bean方法
       * @author Mr li
    　　* @date 2021/8/1 17:59
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    void init();
    /**
       * @things 销毁Bean方法
       * @author Mr li
    　　* @date 2021/8/1 17:59
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    void destory();

    String getResult(double gradle);
}
