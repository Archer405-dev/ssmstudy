package com.litianliu.spring.Dao.Impl;

import com.litianliu.spring.Dao.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

/**
 * @author Mr li
 * @date 2021/8/1 17:54
 */
@Repository
public class UserDaoImpl implements UserDao {
    /**
       * @things 创建spring工程
       * @author Mr li
    　　* @date 2021/8/1 17:56
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    public void save() {
        System.out.println("创建Spring工程...");
    }
    /**
       * @things 初始化Bean
       * @author Mr li
    　　* @date 2021/8/1 17:58
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    public void init(){
        System.out.println("初始化Bean...");
    }

    /**
       * @things 销毁Bean
       * @author Mr li
    　　* @date 2021/8/1 17:58
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    public void destory(){
        System.out.println("销毁Bean...");
    }

    public String getResult(double gradle) {
        if(gradle>=90&&gradle<=100){
            return "优秀";
        }else if(gradle>=80&&gradle<90){
            return "良好";
        }else if(gradle>=70&&gradle<80){
            return "中等";
        }else if(gradle>=60&&gradle<70){
            return "及格";
        }else{
            return "不合格";
        }
    }

}
