package com.litianliu.spring.Entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author tl.li
 * @since 2021/8/9 13:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
//    @Value("11111")
    private String id;
//    @Value("张三")
    private String name;
//    @Value("25")
    private Integer age;
}
