package com.litianliu.spring.Entity;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author tl.li
 * @since 2021/8/10 15:17
 */
@Component
@Data
public class DateSourse {
    @Value("com.mysql.cj.jdbc.driver")
    private String driver;
    @Value("root")
    private String username;
    @Value("llxxuin")
    private String password;
    @Value("jdbc:mysql://www.litianliu.top:3306/ermsSystem")
    private String url;
}
