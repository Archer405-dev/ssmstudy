package com.litianliu.spring.App.Impl;

import com.litianliu.spring.App.Cal;
import org.springframework.stereotype.Component;

/**
 * @author tl.li
 * @since 2021/8/10 19:51
 */
@Component
public class CalImpl implements Cal {
    @Override
    public int add(int num1, int num2) {
        int result=num1+num2;
        return result;
    }
    @Override
    public int sub(int num1, int num2) {
        int result=num1-num2;
        return result;
    }
    @Override
    public int mul(int num1, int num2) {
        int result=num1*num2;
        return result;
    }
    @Override
    public int div(int num1, int num2) {
        int result=num1/num2;
        return result;
    }
}
