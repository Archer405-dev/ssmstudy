package com.litianliu.spring.App;

import java.lang.reflect.Array;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.Arrays;

/**
 * @author tl.li
 * @since 2021/8/11 9:22
 */
public class MyInvocationHandler implements InvocationHandler {
    //委托对象
    private Object object=null;

    //返回代理对象
    public Object bind(Object object){
        this.object=object;
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), object.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //实现业务代码和非业务代码的解耦合
        System.out.println(method.getName()+"方法的入参是："+ Arrays.toString(args));
        Object result=method.invoke(this.object,args);
        System.out.println(method.getName()+"的计算结果是："+result);
        return result;
    }
}
