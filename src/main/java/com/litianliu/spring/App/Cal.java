package com.litianliu.spring.App;

/**
 * @author tl.li
 * @since 2021/8/10 19:49
 */
public interface Cal {
    public int add(int num1,int num2);
    public int sub(int num1,int num2);
    public int mul(int num1,int num2);
    public int div(int num1,int num2);
}
