package com.litianliu.spring.App;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * @author tl.li
 * @since 2021/8/11 10:12
 */
@Component
@Aspect
public class LoggerAspect {
    JoinPoint joinPoint;
    @Before("execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))")
    public void before(JoinPoint joinPoint){
        String name=joinPoint.getSignature().getName();
        String args= Arrays.toString(joinPoint.getArgs());
        System.out.println(name+"方法的入参是："+args);
    }
    @AfterReturning(value = "execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))",returning = "result")
    public void aftreturn(JoinPoint joinPoint,Object result){
        String name=joinPoint.getSignature().getName();
        System.out.println(name+"方法执行的结果是："+result);
    }

    @AfterThrowing(value = "execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))",throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint,Exception ex){
        String name=joinPoint.getSignature().getName();
        System.out.println(name+"方法抛出异常："+ex);
    }
}
