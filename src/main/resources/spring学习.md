# SSM（Spring+SpringMVC+Mybatis）框架学习

# 第一章   Spring框架学习
>@date 2021-08-09
### 1.1  搭建Spring项目的步骤
1.创建Spring坐标，即导入spring依赖
```xml
<!--    pom.xml 添加Spring依赖-->
<dependency>
	<groupId>org.springframework</groupId>
    <artifactId>spring-context</artifactId>
    <version>5.3.7</version> 
</dependency>
```
2.创建Bean<br>
3.创建配置文件applicationContext.xml<br>
4.配置配置文件
```xml
<Bean id="UserDao" class="com.litianliu.spring.UserDao.Impl"></Bean>
```
5.创建applicationContext对象getBean
```java
//applicationContext.xml是我创建的xml配置文件，可根据你自己创建的自定义
public class springTest {
    public static void main(String[] args){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao= (UserDao) app.getBean("userDao");
        userDao.save();
    }
}
```
### 1.2  Spring 配置文件详解
#### 1.2.1 Bean 标签的基本配置 id class<br>
- id 唯一表示Bean实例的，可随意起，但不能重复，一般是类名
- class 指向Bean实例化所在的package包名
#### 1.2.2 Bean 标签的范围配置 scope 理解及对比

scope属性         | singtype    | prototype    |
:-----------------: |:-------- |:-------- |
实例个数         | 1个  | 多个 |
实例时机        | spring核心文件加载时，实例化配置的Bean实例  | 当调用getBean()方法时实例化Bean实例 |
生命周期   | 对象创建：适用对象时创建新的实例对象<br>对象运行：只要对象在使用，对象就一直活着 <br>对象销毁：当应用卸载时，销毁容器时，对象就被销毁了| 对象创建：当应用加载，创建容器时对象就被创建了<br>对象运行：只要容器在，对象就一直活着 <br>对象销毁：当对象长时间不用时，被java的垃圾回收器回收了  |
#### 1.2.3 Bean 生命周期的配置
- init-method：指定类中初始化方法的名称
- destory-method：指定类中销毁方法的名称

#### 1.2.4 Bean 实例化的三种方式
- 无参构造方法实例化
```xml
  <bean id="userDao" class="com.litianliu.spring.Dao.Impl.UserDaoImpl"></bean>
 ```
- 工厂静态方法实例化<br>
(1)xml配置
```xml
  <bean id="user" class="com.litianliu.spring.factory.staticFactory" factory-method="getUser"></bean>
 ```
(2)创建对象
```java
@Data
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private int age;
}
```
(3)静态方法
```java
private static Map<Integer, User> userMap;
static {
    userMap=new HashMap<Integer, User>();
    userMap.put(1,new User("1","张三",22));
    userMap.put(2,new User("2","李四",24));
}
public static User getUser(Integer num){
    return userMap.get(num);
}
```
(4)测试
```java
 @Test
public void staticFactory(){
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    User user= (User) app.getBean("user");
    System.out.println(user);
}
```

- 工厂实例方法实例化

(1)xml配置
```xml
<bean id="dynameic" class="com.litianliu.spring.factory.DynamicFactory"></bean>
<bean id="user" factory-bean="dynameic" factory-method="getUser">
    <constructor-arg value="2"></constructor-arg>
</bean>
```
(2)创建对象
```java
@Data
@AllArgsConstructor
public class User {
    private String id;
    private String name;
    private int age;
}
```
(3)静态方法
```java
public class DynamicFactory {
    private static Map<Integer, User> userMap;
    public DynamicFactory(){
        userMap=new HashMap<Integer, User>();
        userMap.put(1,new User("1","张三",22));
        userMap.put(2,new User("2","李四",24));
    }
    public User getUser(Integer num){
        return userMap.get(num);
    }
}
```
(4)测试
```java
 @Test
public void staticFactory(){
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    User user= (User) app.getBean("user");
    System.out.println(user);
}
```
工厂静态方法和实例方法实例化bean的区别：
静态方法创建对象，不需要实例化工厂对象，因为静态工厂的静态方法，不需要创建对象即可调用，在spring的xml里只需创建一个bean

实例化方法创建对象，需要实例化工厂对象，因为它的方法是静态的，必须要通过实例化才能调用，必须要创建工厂对象，在spring的xml文件中需要创建两个bean，一个是工厂bean，一个是队形bean

## 1.3 spring注解开发
>@date 2021-08-10
### 1.3.1 spring注解开发之bean自动注入
步骤：（1）配置自动扫包
     （2）在类上添加注解
- 在applicationContext.xml中添加扫包代码
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd">
    <context:component-scan base-package="com.litianliu.spring"></context:component-scan>
</beans>
```
- 在类上添加注解

a.创建一个实体类DateSourse.java并添加自动注入@Component，@Date是lombox所带注解，主要作用是自动生成setter，getter方法
```java
@Component
@Data
public class DateSourse {
    @Value("com.mysql.cj.jdbc.driver")
    private String driver;
    @Value("root")
    private String username;
    @Value("llxxuin")
    private String password;
    @Value("jdbc:mysql://www.litianliu.top:3306/ermsSystem")
    private String url;
}
```
注：
- @Value("")是直接给字段赋值

- 自动注入的两种方式：byType和byName，默认都是通过byType的，即名称为类名但是首字母转化为小写


b.创建测试类并验证自动注入是否成功
```java
@Test
public void datesourse() {
    ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
    DateSourse dateSourse= (DateSourse) app.getBean("dateSourse");
    System.out.println(dateSourse);
}
```
component注解是将标注的类加载到容器中，实际开发中可以根据需求使用controller，service，respository来标注控制类你服务类和数据交互类

## 1.4 spring IOC底层实现

技术点：xml解析 + 反射
> 思路：<br>
> 1.根据需求编写 xml 文件，配置需要创建的 bean<br>
> 2.编写程序读取 xml 文件，获取 bean 相关信息、类、属性、id<br>
> 3.根据第二步获取到的信息，结合反射机制动态创建对象，同时完成属性的赋值<br>
> 4.将创建好的 bean 存入Map集合，设置key-value映射，key就是bean中的id值，value就是bean对象<br>
> 5.提供方法从Map中通过id获取到对应的value<br>
```java
//关键代码
package com.litianliu.spring.Ioc;

import com.sun.xml.internal.ws.server.ServerRtException;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.MethodParameter;
import org.springframework.core.ResolvableType;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;

/**
 * @author tl.li
 * @since 2021/8/10 16:48
 */
public class MyClassPathXmlApplicatuonContext implements ApplicationContext {
    private Map<String,Object> map;
    public MyClassPathXmlApplicatuonContext(String path){
        map=new HashMap<String, Object>();
        //解析xml
        parseXML("src/main/resources/"+path);
    }

    private void parseXML(String path) {
        SAXReader saxReader=new SAXReader();
        try {
            Document document=saxReader.read(path);
            Element root=document.getRootElement();
            Iterator<Element> rootIter= root.elementIterator();
            while (rootIter.hasNext()){
                Element bean=rootIter.next();
                String id=bean.attributeValue("id");
                String clazz=bean.attributeValue("class");
                //反射动态创建对象
                Class clazzz=Class.forName(clazz);
                Constructor constructor=clazzz.getConstructor();
                Object object=constructor.newInstance();
                //给属性赋值
                Iterator<Element> beanIter=bean.elementIterator();
                while (beanIter.hasNext()){
                    Element propery=beanIter.next();
                    String properyName=propery.attributeValue("name");
                    String properyValue=propery.attributeValue("value");
                    //获取setter方法
                    //id-setId,name-setName,age-detAge
                    String methodName="set"+properyName.substring(0,1).toUpperCase()+properyName.substring(1);
                    //获取属性类型
                    Field field=clazzz.getDeclaredField(properyName);
                    Method method=clazzz.getMethod(methodName,field.getType());
                    Object value=properyValue;
                    //类型转换
                   switch (field.getType().getName()){
                       case "java.lang.Integer":value=Integer.parseInt(properyValue);
                       break;
                   }
                    //调用方法
                    method.invoke(object,value);
                }
                map.put(id,object);
            }
        }catch (DocumentException e){
            e.printStackTrace();
        }catch (ClassNotFoundException e){
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        }
    }
    public Object getBean(String s) throws BeansException {
        return map.get(s);
    }
}
```
## 1.5 spring AOP
> 概念：AOP 面向切面编程 OOP面向对象编程
> ![img_1.png](img_1.png)
 
### 1.5.1 Spring AOP面向切面编程原理
AOP的优点：
- 降低模块之间的耦合性
- 提高代码的复用性
- 提高代码的维护性
- 方便集中管理非业务代码，便于维护
- 业务代码不收非业务代码的影响

实例(基于原理)

1.创建一个计算器的接口Cal
```java
public interface Cal {
    public int add(int num1,int num2);
    public int sub(int num1,int num2);
    public int mul(int num1,int num2);
    public int div(int num1,int num2);
}
```
2.创建接口的实现类CalImpl
```java
public class CalImpl implements Cal {
    @Override
    public int add(int num1, int num2) {
        return num1+num2;
    }

    @Override
    public int sub(int num1, int num2) {
        return num1-num2;
    }

    @Override
    public int mul(int num1, int num2) {
        return num1*num2;
    }

    @Override
    public int div(int num1, int num2) {
        return num1/num2;
    }
}
```

>@date 2021-08-11

3.需求：在每个方法开始的位置输出参数信息，在每个方法结尾的位置输出结果信息
>对于此计算器来说，加减乘除是业务代码，输出日志信息是非业务代码
- 原始方法： 
```java
  public class CalImpl implements Cal {
    @Override
    public int add(int num1, int num2) {
        System.out.println("此次执行的是加法，传入参数是"+num1+","+num2);
        int result=num1+num2;
        System.out.println("num1+num2="+num1+"+"+num2+"="+result);
        return result;
    }
    @Override
    public int sub(int num1, int num2) {
        System.out.println("此次执行的是减法，传入参数是"+num1+","+num2);
        int result=num1-num2;
        System.out.println("num1-num2="+num1+"-"+num2+"="+result);
        return result;
    }
    @Override
    public int mul(int num1, int num2) {
        System.out.println("此次执行的是乘法，传入参数是"+num1+","+num2);
        int result=num1*num2;
        System.out.println("num1*num2="+num1+"*"+num2+"="+result);
        return result;
    }
    @Override
    public int div(int num1, int num2) {
        System.out.println("此次执行的是除法，传入参数是"+num1+","+num2);
        int result=num1/num2;
        System.out.println("num1/num2="+num1+"/"+num2+"="+result);
        return result;
    }
}
```
- AOP思想：使用动态代理的方式实现。代理首先应该具备calImpl里的所有功能，扩展出打印日志的功能

（1）删除calImpl方法中所有打印日志的方法，只保留业务代码
```java
public class CalImpl implements Cal {
    @Override
    public int add(int num1, int num2) {
        int result=num1+num2;
        return result;
    }
    @Override
    public int sub(int num1, int num2) {
        int result=num1-num2;
        return result;
    }
    @Override
    public int mul(int num1, int num2) {
        int result=num1*num2;
        return result;
    }
    @Override
    public int div(int num1, int num2) {
        int result=num1/num2;
        return result;
    }
}
```

（2）创建一个新类实现InvocationHandler接口，生成动态代理
```java
public class MyInvocationHandler implements InvocationHandler {
    //委托对象
    private Object object=null;

    //返回代理对象
    public Object bind(Object object){
        this.object=object;
        return Proxy.newProxyInstance(object.getClass().getClassLoader(), object.getClass().getInterfaces(),this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //实现业务代码和非业务代码的解耦合
        System.out.println(method.getName()+"方法的入参是："+ Arrays.toString(args));
        Object result=method.invoke(this.object,args);
        System.out.println(method.getName()+"的计算结果是："+result);
        return result;
    }
}
```
(3)测试
```java
@Test
    public void test(){
        Cal cal1=new CalImpl();
        MyInvocationHandler myInvocationHandler=new MyInvocationHandler();
        Cal cal=(Cal)myInvocationHandler.bind(cal1);
        cal.add(10,3);
        cal.sub(10,3);
        cal.mul(10,3);
        cal.div(10,3);
    }
```
测试结果：

![img.png](img.png)

### 1.5.2 Spring AOP在开发中的应用
开发步骤： 前提需要添加 AOP 相关的依赖
```xml
<!--spring aop-->
<dependency>
     <groupId>org.springframework</groupId>
     <artifactId>spring-aspects</artifactId>
     <version>5.3.7</version>
</dependency>
<dependency>
    <groupId>org.springframework</groupId>
    <artifactId>spring-aop</artifactId>
</dependency>
```
1.创建切面类
```java
@Component
@Aspect
public class LoggerAspect {
     JoinPoint joinPoint;
     @Before("execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))")
     public void before(JoinPoint joinPoint){
          String name=joinPoint.getSignature().getName();
          String args= Arrays.toString(joinPoint.getArgs());
          System.out.println(name+"方法的入参是："+args);
     }
     @AfterReturning(value = "execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))",returning = "result")
     public void aftreturn(JoinPoint joinPoint,Object result){
          String name=joinPoint.getSignature().getName();
          System.out.println(name+"方法执行的结果是："+result);
     }

     @AfterThrowing(value = "execution(public int com.litianliu.spring.App.Impl.CalImpl.*(..))",throwing = "ex")
     public void afterThrowing(JoinPoint joinPoint,Exception ex){
          String name=joinPoint.getSignature().getName();
          System.out.println(name+"方法抛出异常："+ex);
     }
}
```
注：注解 @Component, @Aspect, @Before 的含义
- @Component 是将此类注入到 IOC 容器中
- @Aspect 表示该类是该类是一个切面类
- @Before 表示方法的执行时间在业务方法之前 execution表示切入点是 CalImpl
- @After 表示方法的执行时间在业务方法执行结束之后
- @AfterReturning 表示方法的执行时间在业务方法返回结果之后 returning 将业务方法的返回值与切面方法的形参进行绑定
- @AfterThrowing 表示方法的执行实际是在业务方法执行抛出异常之后  Throwing 将业务方法的异常与切面方法的形参进行绑定


2.将委托类注入到 ioc 容器，即添加 @Component
```java
@Component
public class CalImpl implements Cal {
    @Override
    public int add(int num1, int num2) {
        int result=num1+num2;
        return result;
    }
    @Override
    public int sub(int num1, int num2) {
        int result=num1-num2;
        return result;
    }
    @Override
    public int mul(int num1, int num2) {
        int result=num1*num2;
        return result;
    }
    @Override
    public int div(int num1, int num2) {
        int result=num1/num2;
        return result;
    }
}
```
3.配置xml文件
```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:context="http://www.springframework.org/schema/context"
       xmlns:aop="http://www.springframework.org/schema/aop"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd http://www.springframework.org/schema/context https://www.springframework.org/schema/context/spring-context.xsd http://www.springframework.org/schema/aop https://www.springframework.org/schema/aop/spring-aop.xsd">
    <!--自动扫包-->
    <context:component-scan base-package="com.litianliu.spring"></context:component-scan>
    <!--为委托对象自动生成代理对象-->
    <aop:aspectj-autoproxy></aop:aspectj-autoproxy>
</beans>
```
注：<aop:aspectj-autoproxy，spring IOC容器会结合切面对象和委托对象自动生成代理对象，AOP底层就是通过动态代理机制来实现的。

总结AOP的概念：
- 切面对象：根据切面抽象出来的对象，CalImpl 所有方法中要加入日志的部分，抽象成一个切面类LoggerAspect
- 通知：切面对象具体执行的代码，即非业务代码
- 目标：被横切的对象，即委托对象，将通知加入其中
- 代理：切面对象、通知、目标混合之后的结果，即我们使用JDK动态代理动态创建对象
- 连接点：需要被横切的位置，即通知要插入业务代码的具体位置

# 第二章   SpringMVC 框架学习
>SpringMVC 是 Spring Framework 提供的 web 组件，全称是 Spring Web MVC，是目前主流的设计模式框架，提供前端路由银蛇、视图解析等功能

### 2.1  SpringMVC 功能·




