import com.litianliu.spring.Entity.DateSourse;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author tl.li
 * @since 2021/8/10 15:19
 */
public class dateSourseTest {
    @Test
    public void datesourse() {
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        DateSourse dateSourse= (DateSourse) app.getBean("dateSourse");
        System.out.println(dateSourse);
    }
}
