import com.litianliu.spring.App.Cal;
import com.litianliu.spring.App.Impl.CalImpl;
import com.litianliu.spring.App.MyInvocationHandler;
import org.junit.Test;

import java.util.Map;

/**
 * @author tl.li
 * @since 2021/8/10 19:56
 */
public class CalTest {
    @Test
    public void test(){
        Cal cal1=new CalImpl();
        MyInvocationHandler myInvocationHandler=new MyInvocationHandler();
        Cal cal=(Cal)myInvocationHandler.bind(cal1);
        cal.add(10,3);
        cal.sub(10,3);
        cal.mul(10,3);
        cal.div(10,3);
    }
}
