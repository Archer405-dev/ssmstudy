import com.litianliu.spring.Entity.User;
import com.litianliu.spring.Ioc.MyClassPathXmlApplicatuonContext;
import org.junit.Test;
import org.springframework.context.ApplicationContext;

/**
 * @author tl.li
 * @since 2021/8/10 16:58
 */
public class IocTest {
    @Test
    public void readXml(){
        ApplicationContext app=new MyClassPathXmlApplicatuonContext("spring-ioc.xml");
        User user= (User) app.getBean("user1");
        System.out.println(user);
    }
}
