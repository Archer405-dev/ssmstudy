import com.litianliu.spring.Entity.User;
import com.litianliu.spring.controller.UserController;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author tl.li
 * @since 2021/8/10 8:55
 */
public class userControllerTest {
    @Test
    public void getUser(){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserController user= (UserController) app.getBean("userController");
        int m=0;
        String[] str=new String[11];
        for (int i = 0; i <= 10; i++) {
            m=i*10;
            str[i]=user.getUser(m);
        }
        for (int i = 0; i < str.length; i++) {
            System.out.println(str[i]);
        }
    }
}
