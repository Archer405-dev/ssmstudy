import com.litianliu.spring.App.Cal;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;

/**
 * @author tl.li
 * @since 2021/8/11 10:37
 */
public class AOPTest {
    @Test
    public void aop(){
        ApplicationContext app=new ClassPathXmlApplicationContext("spring-aop.xml");
        Cal cal=(Cal) app.getBean("calImpl");
        cal.add(10,3);
        cal.sub(10,3);
        cal.mul(10,3);
        cal.div(10,5);
    }
    @Test
    public void test(){
        String sql="INSERT INTO DOP(PK_ID,share) values(_sql_param_0,_sql_param_1)";
        int[] sqqlTypes=new int[]{12,3};
        Object[] data=new String[]{"0000000448543.87",""};
        for(int i = 0; i < data.length; ++i) {
            if (data[i] == null ) {
                sql = sql.replaceFirst("_sql_param_" + i, "null");
            } else if (sqqlTypes[i] == 12) {
                String value = "'" + data[i].toString() + "'";
                sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(value));
            } else if (sqqlTypes[i] == 93) {
                DateFormat formatter = DateFormat.getDateTimeInstance(2, 2, Locale.CHINA);
                String value = "'" + formatter.format((Date)data[i]) + "'";
                sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(value));
            } else {
                if(sqqlTypes[i] == 3 && data[i].toString().length() == 0){
                    sql = sql.replaceFirst("_sql_param_" + i, "null");
                }else{
                    sql = sql.replaceFirst("_sql_param_" + i, Matcher.quoteReplacement(data[i].toString()));
                }
            }
        }
        System.out.println(sql);
    }
}
