import com.litianliu.spring.Dao.UserDao;
import com.litianliu.spring.Entity.User;
import com.litianliu.spring.factory.DynamicFactory;
import com.litianliu.spring.factory.staticFactory;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Mr li
 * @date 2021/8/1 16:29
 */
public class springTest {
    /**
       * @things spring搭建测试
                 applicationContext.xml代码如下：
                 <bean id="userDao" class="com.litianliu.spring.UserDao.Impl.UserDaoImpl"></bean>
       * @author Mr li
    　　* @date 2021/8/1 16:49
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    @Test
    public void creatSpring(){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao= (UserDao) app.getBean("userDao");
        userDao.save();
    }

    /**
       * @things 测试 Bean 标签 scope 属性
       *         两个属性：singleton prototype
                 applicationContext.xml代码如下：
                 第一种 singtype：<bean id="userDao" class="com.litianliu.spring.UserDao.Impl.UserDaoImpl" scope="singtype"></bean>
       * @author Mr li
    　　* @date 2021/8/1 16:50
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    @Test
    public void singScope(){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao1= (UserDao) app.getBean("userDao1");
        UserDao userDao2= (UserDao) app.getBean("userDao1");
        System.out.println(userDao1);
        System.out.println(userDao2);
    }

    /**
     * @things 测试 Bean 标签 scope 属性
     *         两个属性：singleton prototype
    applicationContext.xml代码如下：
    第二种 prototype：<bean id="userDao" class="com.litianliu.spring.UserDao.Impl.UserDaoImpl" scope="prototype"></bean>
     * @author Mr li
    　　* @date 2021/8/1 16:50
    　　* @param * @param null
    　　* @return * @return {@link null }
     */
    @Test
    public void protoScope(){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao1= (UserDao) app.getBean("userDao2");
        UserDao userDao2= (UserDao) app.getBean("userDao2");
        System.out.println(userDao1);
        System.out.println(userDao2);
    }

    /**
       * @things 测试Bean初始化init-method和销毁方法destory-method
       *  applicationContext.xml代码如下：
          <bean id="userDao" class="com.litianliu.spring.UserDao.Impl.UserDaoImpl" init-method="init" destroy-method="destory"></bean>
       * @author Mr li
    　　* @date 2021/8/1 17:36
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    @Test
    public void initanddestory(){
        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao userDao= (UserDao) app.getBean("userDao3");
        System.out.println(userDao);
        ((ClassPathXmlApplicationContext) app).close();
    }
    /**
       * @things 静态工厂实例化Bean
       * @author Mr li
    　　* @date 2021/8/1 18:29
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    @Test
    public void staticFactory(){
//        UserDao user=staticFactory.getUserDao();
//        System.out.println(user);

//        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
//        UserDao userDao= (UserDao) app.getBean("userDao4");
//        System.out.println(userDao);

//        静态方法实例化bean
//        User user= staticFactory.getUser(2);
//        System.out.println(user);

        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        User user= (User) app.getBean("user");
        System.out.println(user);

    }
    /**
       * @things 工厂实例化实例Bean
       * @author Mr li
    　　* @date 2021/8/1 18:35
    　　* @param * @param null
    　　* @return * @return {@link null }
    */
    @Test
    public void dynamicFactory(){
//        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
//        UserDao userDao= (UserDao) app.getBean("userDao5");
//        System.out.println(userDao);

        //实例化工厂类常规方式
//        DynamicFactory dynamicFactory=new DynamicFactory();
//        User user=dynamicFactory.getUser(2);
//        System.out.println(user);
        //实例化工厂类bean方式

        ApplicationContext app=new ClassPathXmlApplicationContext("applicationContext.xml");
        User user= (User) app.getBean("user");
        System.out.println(user);
    }
}
